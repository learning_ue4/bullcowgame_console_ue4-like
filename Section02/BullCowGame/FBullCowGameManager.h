#pragma once
#include <string>
#include "FBullCowGame.h"

// ���������� ����� Unreal
using FString = std::string;
using int32 = int;
template<class T> using TArray = std::vector<T>;

class FBullCowGameManager;

class FBullCowGameManagerDestroyer
{
public:
	~FBullCowGameManagerDestroyer();
	void Initialize(FBullCowGameManager* Instance);
	
private:
	FBullCowGameManager* Instance = nullptr;	
};

class FBullCowGameManager
{
public:	
	static FBullCowGameManager& GetInstance(std::ostream& OutStream, std::istream& InStream);
	void StartGame(const bool& bIsShowIntroNeeded);
	
protected:
	explicit FBullCowGameManager(std::ostream& OutStream, std::istream& InStream);	
	friend class FBullCowGameManagerDestroyer;

private:
	std::ostream& OutputStream;
	std::istream& InputStream;
	static FBullCowGameManager* Instance;
	static FBullCowGameManagerDestroyer Destroyer;
	FBullCowGame* BCGame;

	const FString QuitCommands[3] = { "quit", "Quit", "QUIT" };
	
	void ShowIntro();
	void SetDesiredWordLength(); 
	void PrepareGame();
	void PrintGameSummary() const;
	void PrintGameError(const EResetStatus& Status);

	// ��������� ������ ����� � ����
	FString GetGuess();

	// ����� �� ����� ����� �� ����?
	void CheckWillingToQuit(const FString& Input);

	// ����� �� ����� ������� �����?
	bool IsPlayAgainConfirmed() const;

	// ��������������� ����
	void PlayGame();	
};
