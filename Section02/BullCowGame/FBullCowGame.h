/*	
 *	������� ������.
 *	��� �������������� � �������������.
 */
#pragma once
#include <map>
#include <vector>
#include <string>

 // ���������� ����� Unreal
#define TMap std::map
using FString = std::string;
using int32 = int;
template<class T> using TArray = std::vector<T>;

/*	
 *	���������, ������������ ���������� ����� � ����� ������. 
 *	���������� � ���� �� �����, �.�. �� ���������� ��� � main, ��� ��������������� �����
 *	��� ������ FBullCowGame 
 */
struct FBullCowCount
{
	int32 Bulls = 0;
	int32 Cows = 0;
};

/*	
 *	������, ������������ ��� ������. 
 *	���������� ��������� ������:
 *	�������� ������, ��, �� ���������, �������� �����,
 *	�� ������ �������, �������� ���������� ����,
 *	�������� �������
 */
enum class EGuessStatus
{
	InvalidStatus,
	OK,
	NotIsogram,
	WrongLength,
	NotLowercase,
	MultipleWords,
	WrongSymbols
};

/*	
 *	������, ������������ ����� ������ ����,
 *	����������� ����������, �������� �� � ����������� 
 *	��� ������
 */
enum class EResetStatus
{
	OK,
	GameNotSet,
	NoHiddenWordsFound,
	IsogramCorruption,
	NoHiddenWordFilesFound	
};

/*	
 *	�������� �����, ������������ ������� ����  
 */
class FBullCowGame
{
public:	
	FBullCowGame();	

	int32 GetMinWordLengthAvailable() const;
	int32 GetMaxWordLengthAvalable() const;
	int32 GetMaxTries() const;
	int32 GetCurrentTry() const;
	int32 GetHiddenWordLength() const;
	int32 GetMyCurrentWordLength() const;
	void SetMyCurrentWordLength(const int32& WordLength);
	FString GetCurrentHiddenWord() const;
	EResetStatus GetGameStatus() const;
	bool IsGameWon() const;	
	FString GetWinMessage() const;
	FString GetLostMessage() const;
	TArray<FString> GetIntroMessage() const;
	void Reset(const int32& HiddenWordLength = 5);	
	TArray<EGuessStatus> CheckGuessValidity(const FString& Guess) const;
	// �����, ��������� ���������� ����� � ����� � ���������������� # ������� ��� �������� �������
	FBullCowCount SubmitValidGuess(const FString& Guess);

private:
	FString GameWonMessage = "YOU WON!! CONGRATULATIONS!!!";
	FString GameOverMessage = "Game Over! Better luck next time...";
	int32 MinWordLengthAvailable = 4;
	int32 MaxWordLengthAvailable = 10;
	TMap<int32, FString> HiddenWordFiles 
	{
		{4, "words4.txt"},
		{5, "words5.txt"},
		{6, "words6.txt"},
		{7, "words7.txt"},
		{8, "words8.txt"},
		{9, "words9.txt"},
		{10, "words10.txt"}
	};
	EResetStatus CurrentStatus = EResetStatus::OK;
	int32 MyCurrentTry = 0;
	int32 MyCurrentWordLength = 0;
	int32 MyHiddenWordLength = 0;
	bool bIsGameWon = false;

	std::vector<FString> MyHiddenWords;
	FString CurrentHiddenWord;	

	bool IsIsogram(const FString& Word) const;
	bool IsLowerCase(const FString& Word) const;

	TArray<FString> GetHiddenWords(const int32& WordLength);
	FString GetRandomHiddenWord();	
};
