// BullCowGame.cpp : Defines the entry point for the console application.
//
/* ����������� ������ ����������� ����������, ������������ ����� BullCowGame,
 * ������ ���� ����� � MVC, �������� �� �������������� ������������.
 * ��� ������ �������� �������� ��. FBullCowGame.
 */

#include "stdafx.h"
#include "FBullCowGameManager.h"
#include <iostream>

int main()
{	
	auto GameManager = FBullCowGameManager::GetInstance(std::cout, std::cin);
	GameManager.StartGame(true);
}
