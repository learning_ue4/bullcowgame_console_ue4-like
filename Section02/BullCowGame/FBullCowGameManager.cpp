#include "stdafx.h"
#include <iostream>
#include "FBullCowGame.h"
#include "FBullCowGameManager.h"

// ���������� ����� Unreal
using FString = std::string;
using int32 = int;
template<class T> using TArray = std::vector<T>;

FBullCowGameManager* FBullCowGameManager::Instance = nullptr;
FBullCowGameManagerDestroyer FBullCowGameManager::Destroyer;

FBullCowGameManagerDestroyer::~FBullCowGameManagerDestroyer()
{
	delete Instance->BCGame;
	delete Instance;
}

void FBullCowGameManagerDestroyer::Initialize(FBullCowGameManager* Instance)
{
	this->Instance = Instance;
}

FBullCowGameManager::FBullCowGameManager(std::ostream& OutStream, std::istream& InStream) :
	OutputStream(OutStream),
	InputStream(InStream)
{
	BCGame = new FBullCowGame();
}

FBullCowGameManager& FBullCowGameManager::GetInstance(std::ostream& OutStream, std::istream& InStream)
{
	if (!Instance)
	{
		Instance = new FBullCowGameManager(OutStream, InStream);
		Destroyer.Initialize(Instance);
	}

	return *Instance;
}

void FBullCowGameManager::StartGame(const bool& bIsShowIntroNeeded)
{
	if (bIsShowIntroNeeded)
	{
		ShowIntro();
		Instance->OutputStream << "\nP.S. If you want to quit the game, simply type 'quit' (or 'Quit', or 'QUIT')\n\n";
	}	

	bool bWantsToPlayAgain = false;
	do
	{
		PrepareGame();
		PlayGame();
		PrintGameSummary();
		bWantsToPlayAgain = IsPlayAgainConfirmed();
	}
	while (bWantsToPlayAgain);

	return;
}

void FBullCowGameManager::ShowIntro()
{
	const auto InfoMessage = BCGame->GetIntroMessage();

	for (const auto& String : InfoMessage)
	{
		Instance->OutputStream << String << "\n";
	}
}

void FBullCowGameManager::SetDesiredWordLength()
{
	const auto MinWordLength = BCGame->GetMinWordLengthAvailable();
	const auto MaxWordLength = BCGame->GetMaxWordLengthAvalable();
	const auto bIsDesiredWordLengthSet = false;

	do
	{
		FString Answer = "";
		Instance->OutputStream << "Enter desired word length from " << MinWordLength << " to " << MaxWordLength << ": ";
		
		std::getline(Instance->InputStream, Answer);
		
		if (Instance->InputStream.fail())
		{
			Instance->OutputStream << "Incorrect input! Enter a number between " << MinWordLength << " and " <<
				MaxWordLength << "!\n";
			Instance->InputStream.clear();
			Instance->InputStream.ignore(32767, '\n');
		}
		else
		{
			CheckWillingToQuit(Answer);
			try
			{
				auto DesiredWordLength = std::stoi(Answer);

				if (DesiredWordLength < MinWordLength || DesiredWordLength > MaxWordLength)
				{
					Instance->OutputStream << "Incorrect value entered!\n";
					Instance->InputStream.clear();
					Instance->InputStream.ignore(32767, '\n');
				}
				else
				{
					Instance->OutputStream << "You've chosen to play with " << DesiredWordLength <<
						" letters. Let's go!\n\n";
					FBullCowGameManager::BCGame->SetMyCurrentWordLength(DesiredWordLength);
					return;
				}
			}
			catch (std::exception& Ex)
			{
				Instance->OutputStream << "Incorrect input! Enter a number between " << MinWordLength << " and " <<
					MaxWordLength << "!\n";
				Instance->InputStream.clear();
				Instance->InputStream.ignore(32767, '\n');
			}
		}
	}
	while (!bIsDesiredWordLengthSet);
}

void FBullCowGameManager::PrepareGame()
{
	SetDesiredWordLength();
	const auto DesiredWordLength = BCGame->GetMyCurrentWordLength();
	BCGame->Reset(DesiredWordLength);
}

void FBullCowGameManager::PrintGameSummary() const
{
	if (BCGame->IsGameWon())
	{
		Instance->OutputStream << BCGame->GetWinMessage() << "\n";
	}
	else
	{
		Instance->OutputStream << "\n" << BCGame->GetLostMessage() << "\n";
		Instance->OutputStream << "The hidden word was " << "<" << BCGame->GetCurrentHiddenWord() << "> !\n";
	}
}

void FBullCowGameManager::PrintGameError(const EResetStatus& Status)
{
	switch (Status)
	{
	case EResetStatus::IsogramCorruption:
		Instance->OutputStream << "ERROR! Hidden word is not an isogram, check your game files!\n";
		break;
	case EResetStatus::NoHiddenWordsFound:
		Instance->OutputStream << "ERROR! No hidden words of the desired length found, check your game files!\n";
		break;
	case EResetStatus::NoHiddenWordFilesFound:
		Instance->OutputStream << "ERROR! No files with hidden words of desired length found!\n";
		break;
	case EResetStatus::GameNotSet:
		Instance->OutputStream << "ERROR! Game settings not implemented!\n";
		break;
	default: break;
	}
}

// �������, ���� ���� �� ���� ���������� �����
FString FBullCowGameManager::GetGuess()
{
	auto Statuses = std::vector<EGuessStatus>();	
	FString Guess = "";
	do
	{
		const int32 MyCurrentTry = BCGame->GetCurrentTry();
		Instance->OutputStream << "Try #" << MyCurrentTry << " of " << BCGame->GetMaxTries() << " .";
		Instance->OutputStream << "Enter your answer: ";
		std::getline(Instance->InputStream, Guess);
		
		CheckWillingToQuit(Guess);

		Statuses = BCGame->CheckGuessValidity(Guess);

		for (const auto& Status : Statuses)
		{
			switch (Status)
			{
			case EGuessStatus::WrongLength:
				Instance->OutputStream << "Wrong word length! Please enter a ";
				Instance->OutputStream << BCGame->GetHiddenWordLength();
				Instance->OutputStream << " letter word length";
				break;
			case EGuessStatus::NotIsogram:
				Instance->OutputStream << "Please enter an isogram (a word w/o repeating letters)";
				break;
			case EGuessStatus::NotLowercase:
				Instance->OutputStream << "Please enter only lower-case letters";
				break;
			case EGuessStatus::MultipleWords:
				Instance->OutputStream << "Please enter only one word";
				break;
			case EGuessStatus::WrongSymbols:
				Instance->OutputStream << "Please enter only latin letters";
				break;
			default:
				return Guess;
			}
			Instance->OutputStream << "\n";
		}
		Instance->OutputStream << "\n";
	}
	while (!Statuses.empty()); // �������, ���� �� �������� ���������� �����	
	return Guess;
}

void FBullCowGameManager::CheckWillingToQuit(const FString& Input)
{
	const auto Result = std::find(std::begin(QuitCommands), std::end(QuitCommands), Input);
	if (Result != std::end(QuitCommands))
	{		
		if (IsPlayAgainConfirmed())
		{
			StartGame(false);
		}
		else
		{
			exit(0);
		}
	}
}

// �������� ������� ������� ��� ���
bool FBullCowGameManager::IsPlayAgainConfirmed() const
{
	auto bResult = false;
	auto bIsAnswerCorrect = false;
	do
	{
		Instance->OutputStream << "Do you want to play again (Y/N) ?";
		FString Response = "";
		std::getline(Instance->InputStream, Response);

		if (Response[0] == 'y' || Response[0] == 'Y')
		{
			bResult = true;
			bIsAnswerCorrect = true;
		}
		else if (Response[0] == 'n' || Response[0] == 'N')
		{
			Instance->OutputStream << "Thank You For Playing!!" << "\n";
			bResult = false;
			bIsAnswerCorrect = true;
		}
		else
		{			
			Instance->OutputStream << "Answer not recognized. \n";
			bIsAnswerCorrect = false;
		}
	}
	while (!bIsAnswerCorrect);

	Instance->OutputStream << "\n";
	return bResult;
}


void FBullCowGameManager::PlayGame()
{
	const auto GameStatus = BCGame->GetGameStatus();
	if (GameStatus != EResetStatus::OK)
	{
		PrintGameError(GameStatus);
		return;
	}

	const auto MaxTries = BCGame->GetMaxTries();

	while (!BCGame->IsGameWon() &&
		BCGame->GetCurrentTry() <= MaxTries)
	{
		const auto Guess = GetGuess();
		if (!Guess.empty())
		{			
			const auto BCCount = BCGame->SubmitValidGuess(Guess);
			Instance->OutputStream << "Bulls = " << BCCount.Bulls << " \n";
			Instance->OutputStream << "Cows = " << BCCount.Cows << " \n\n";
		}
	}
}
