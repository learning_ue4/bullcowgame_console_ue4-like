#include "stdafx.h"
#include <map>
#include <cctype>
#include <fstream>
#include <string>
#include "FBullCowGame.h"
#include <ctime>

// ���������� ����� Unreal
#define TMap std::map
using FString = std::string;
using int32 = int;
template<class T> using TArray = TArray<T>;

FBullCowGame::FBullCowGame()
{	
	CurrentStatus = EResetStatus::GameNotSet;
}

int32 FBullCowGame::GetMinWordLengthAvailable() const { return MinWordLengthAvailable; }
int32 FBullCowGame::GetMaxWordLengthAvalable() const { return MaxWordLengthAvailable; }

int32 FBullCowGame::GetMaxTries() const
{
	return MyHiddenWordLength * 3;
}

int32 FBullCowGame::GetCurrentTry() const { return MyCurrentTry; }
int32 FBullCowGame::GetHiddenWordLength() const { return MyHiddenWordLength; }
int32 FBullCowGame::GetMyCurrentWordLength() const { return MyCurrentWordLength; }

void FBullCowGame::SetMyCurrentWordLength(const int32& WordLength)
{
	MyCurrentWordLength = WordLength;
}

FString FBullCowGame::GetCurrentHiddenWord() const { return CurrentHiddenWord; }
EResetStatus FBullCowGame::GetGameStatus() const { return CurrentStatus; }
bool FBullCowGame::IsGameWon() const {	return bIsGameWon; }
FString FBullCowGame::GetWinMessage() const { return GameWonMessage; }
FString FBullCowGame::GetLostMessage() const { return GameOverMessage; }

// �������������� ��������� � ��������� ����
TArray<FString> FBullCowGame::GetIntroMessage() const
{
	auto Result = TArray<FString>();

	std::ifstream InputFile("intro.txt");
	if (!InputFile)
	{		
		return Result;
	}

	FString IntroMessagePart;
	while(std::getline(InputFile, IntroMessagePart))
	{
		Result.push_back(IntroMessagePart);
	}

	return Result;
}

// ����� ���� � �������� ������ �����
void FBullCowGame::Reset(const int32& HiddenWordLength)
{
	MyCurrentTry = 1;
	MyHiddenWordLength = HiddenWordLength;
	MyHiddenWords = GetHiddenWords(HiddenWordLength);

	if (MyHiddenWords.empty())
	{
		CurrentStatus = EResetStatus::NoHiddenWordsFound;
		return;
	}	
	
	CurrentHiddenWord = GetRandomHiddenWord();

	if (!IsIsogram(CurrentHiddenWord))
	{
		CurrentStatus = EResetStatus::IsogramCorruption;
		return;
	}

	bIsGameWon = false;	
	CurrentStatus = EResetStatus::OK;
}

// �������� �� ������������ ��������� ������
TArray<EGuessStatus> FBullCowGame::CheckGuessValidity(const FString& Guess) const
{
	auto Statuses = TArray<EGuessStatus>();

	if (!IsIsogram(Guess))
	{
		Statuses.push_back(EGuessStatus::NotIsogram);
	}
	
	if (!IsLowerCase(Guess))
	{
		Statuses.push_back(EGuessStatus::NotLowercase);
	}
	
	if (Guess.length() != GetHiddenWordLength())
	{
		Statuses.push_back(EGuessStatus::WrongLength);
	}	

	return Statuses;
}

// ��������� ��������� ������, ��������� ���� � ����������� �-�� ����� � �����
FBullCowCount FBullCowGame::SubmitValidGuess(const FString& Guess)
{	
	++MyCurrentTry;
	
	FBullCowCount BCCount;
	BCCount.Bulls = 0;
	BCCount.Cows = 0;

	FString HiddenWord = CurrentHiddenWord;
	int32 WordLength = HiddenWord.length(); //��������������, ��� ����� � ���������� ����� ����� �����	
	
	for (int32 i = 0; i < WordLength; i++)
	{		
		for (int32 j = 0; j < WordLength; j++)
		{			
			if (Guess[i] == HiddenWord[j])
			{				
				if (i == j)
				{
					++BCCount.Bulls;
					continue;
				}
				++BCCount.Cows;				
			}			
		}
	}	
	
	bIsGameWon = (BCCount.Bulls == WordLength);
	return BCCount;
}

// ��������, �������� �� ����� ����������
bool FBullCowGame::IsIsogram(const FString& Word) const
{
	// ����� ������ 0 � 1 ��������� �����������
	if (Word.length() <= 1) { return true; }
	
	auto LetterSeen = TMap<char, bool>();
	
	for (auto Letter : Word) 
	{
		Letter = tolower(Letter);
		if (LetterSeen[Letter]) { return false;	}

		LetterSeen[Letter] = true;
	}
	return true;
}

// ��������, ��� �� ����� � ������ ��������
bool FBullCowGame::IsLowerCase(const FString& Word) const
{
	for (auto Letter : Word)
	{
		if (!islower(Letter)) { return false; }
	}
	return true;
}

// ��������� ������ ����
TArray<FString> FBullCowGame::GetHiddenWords(const int32& WordLength)
{
	if (HiddenWordFiles.count(WordLength) == 0)
	{
		return TArray<FString>();
	}

	FString fileName = HiddenWordFiles[WordLength];	

	auto Result = TArray<FString>();
	std::ifstream InputFile(fileName);
	if (!InputFile)
	{	
		return Result;
	}
	
	FString CurrentWord;
	while (std::getline(InputFile, CurrentWord))
	{
		Result.push_back(CurrentWord);
	}

	return Result;
}

//��������� ����� ����� �� ������ ������� ���� 
FString FBullCowGame::GetRandomHiddenWord()
{
	srand(time(nullptr));
	int32 index = std::rand() % MyHiddenWords.size();
	return MyHiddenWords[index];
}
