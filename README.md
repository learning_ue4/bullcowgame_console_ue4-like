# Bullcowgame Console Ue4 Like

An implementation of a famous word game "Bulls and Cows" as a console app
with imitation of UE4 coding style.

Made as a part of a Udemy course "Unreal Engine C++ Developer" https://www.udemy.com/course/unrealcourse/ with some additions: full game rules and complete game loop,
altered project design, etc
